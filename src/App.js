import LayoutOne from "./Layout/LayoutOne";

function App() {
  return (
    <div className="App">
      <LayoutOne />
    </div>
  );
}

export default App;
