import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <div
        className="bg-secondary text-center pt-4 mt-3"
        style={{ height: "80px" }}
      >
        Copyright © Your Website 2022
      </div>
    );
  }
}
