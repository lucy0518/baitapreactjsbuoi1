import React, { Component } from "react";

export default class Banner extends Component {
  title = "";
  render() {
    return (
      <div>
        <div className="card text-center ">
          <div className="card-body ">
            <h1>A warm welcome! </h1>
            <h5 className="card-title">{this.title}</h5>
            <p className="card-text">
              Bootstrap utility classes are used to create this jumbotron since
              the old component has been removed from the framework. Why create
              custom CSS when you can use utilities?
            </p>
            <a href="#" className="btn btn-primary">
              Call to action
            </a>
          </div>
        </div>
      </div>
    );
  }
}
