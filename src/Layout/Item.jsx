import React, { Component } from "react";

export default class Item extends Component {
  title = "Card title";
  render() {
    let cardStyle = { width: "18 rem" };
    let imgSrc = "https://picsum.photos/200/300";
    return (
      <div className="row mt-2 ">
        <div className="col-4 w-25">
          <div className="card" style={cardStyle}>
            <div className="card-body ">
              <img
                className="card-img-top w-100 "
                src={imgSrc}
                alt="Card image cap"
              />
              <h5 className="card-title">{this.title}</h5>
              <h3> Fresh new layout </h3>
              <p className="card-text">
                With Bootstrap 5, we've created a fresh new layout for <br />{" "}
                this template!
              </p>
              <a href="#" className="btn btn-primary">
                Go somewhere
              </a>
            </div>
          </div>
        </div>
        <div className="col-4 w-25">
          <div className="card" style={cardStyle}>
            <div className="card-body ">
              <img
                className="card-img-top w-100"
                src={imgSrc}
                alt="Card image cap"
              />
              <h5 className="card-title">{this.title}</h5>
              <h3> Free to download </h3>
              <p className="card-text">
                As always, Start Bootstrap has a powerful of
                <br /> Sunny
                <br /> free templates.
              </p>
              <a href="#" className="btn btn-primary">
                {" "}
                Go somewhere
              </a>
            </div>
          </div>
        </div>
        <div className="col-4 w-25">
          <div className="card" style={cardStyle}>
            <div className="card-body">
              <img
                className="card-img-top w-100"
                src={imgSrc}
                alt="Card image cap"
              />
              <h5 className="card-title">{this.title}</h5>
              <h3> Jumbotron hero header </h3>
              <p className="card-text">
                The heroic part of this template is the jumbotron hero <br />{" "}
                header!
              </p>
              <a href="#" className="btn btn-primary">
                {" "}
                Go somewhere
              </a>
            </div>
          </div>
        </div>
        <div className="col-4 w-25">
          <div className="card" style={cardStyle}>
            <div className="card-body">
              <img
                className="card-img-top w-100"
                src={imgSrc}
                alt="Card image cap"
              />
              <h5 className="card-title">{this.title}</h5>
              <h3> Feature boxes </h3>
              <p className="card-text">
                We've created some custom feature boxes using <br /> Bootstrap
                icons!
              </p>
              <a href="#" className="btn btn-primary">
                {" "}
                Go somewhere
              </a>
            </div>
          </div>
        </div>
        <div className="col-4 w-25">
          <div className="card" style={cardStyle}>
            <div className="card-body">
              <img
                className="card-img-top w-100"
                src={imgSrc}
                alt="Card image cap"
              />
              <h5 className="card-title">{this.title}</h5>
              <h3> Simple clean code </h3>
              <p className="card-text">
                We keep our dependencies up to date and squash bugs <br /> as
                they come!
              </p>
              <a href="#" className="btn btn-primary">
                {" "}
                Go somewhere
              </a>
            </div>
          </div>
        </div>
        <div className="col-4 w-25">
          <div className="card" style={cardStyle}>
            <div className="card-body">
              <img
                className="card-img-top w-100"
                src={imgSrc}
                alt="Card image cap"
              />
              <h5 className="card-title">{this.title}</h5>
              <h3> A name you trust </h3>
              <p className="card-text">
                Start Bootstrap has been the leader in free Bootstrap <br />{" "}
                templates since 2013!
              </p>
              <a href="#" className="btn btn-primary">
                {" "}
                Go somewhere
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
