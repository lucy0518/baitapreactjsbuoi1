import React, { Component } from "react";
import Banner from "./Banner";
import Footer from "./Footer";
import Header from "./Header";
import Item from "./Item";

export default class LayoutOne extends Component {
  render() {
    return (
      <div className="container">
        <Header />
        <Banner />
        <Item />

        <Footer />
      </div>
    );
  }
}
